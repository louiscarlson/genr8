""" defines the ProcMacher class which controls the making of individual
procedures (via proc.py)
"""
import os
import dbhelper.my_sql as MySQL
import genr8.proc



class ProcMacher():
    """
    looks up meta data about a table and creates two versions of four basic procs

        save (handles inserts and updates)
        select
        delete
        dupeCheck

        one each for a variable input version and a literal input version

        usage var = proc_macher(table_name, table_schema)

    """

    simple_inputs = ['delete', 'select']
    date_added_col_name = 'date_added'
    delimiter = '||'
    delimiter_in = 'delimiter ' + delimiter
    delimiter_out = 'delimiter ;'

    def make_column_list(self, proc, usage):

        """
        no comment.

        """

        out_col_list = []
        skip_date_added_col = False

        if usage == 'call':
            # this is only done when making the dedupe proc call
            # it needs diff cols than the insert or update procs that call it
            col_list_type = 'full'
        elif usage == 'execute':
            # never used for execute, except as added by the proc
            # for select, update, delete in the WHERE
            col_list_type = 'NonPK'
        elif proc.action == 'insert' and proc.input_literals:
            # literal insert doesn't pass in the pk
            col_list_type = 'NonPK'
        elif proc.action in ['select', 'delete'] and proc.input_literals:
            # these two have only the pk as input
            col_list_type = 'PK'
        elif proc.action == 'delete' and not proc.input_literals:
            # just the PK
            col_list_type = 'PK'
        else:
            # use them all
            col_list_type = 'full'
        if proc.action == 'update' and usage in ['execute']:
        # skip the date_added column when executing the update
            skip_date_added_col = True
        use_col = 'COLUMN_NAME'
        extrees = proc.extra_cols
        if usage == 'define':
            use_col = 'COL_W_DATA' # include the data type
            extrees = proc.extra_cols_type

        for this_col in self.macher.dat.results:
            this_col_name = this_col['COLUMN_NAME']
            if this_col_name == self.date_added_col_name and skip_date_added_col:
                pass
            elif col_list_type == 'full':
                out_col_list.append(this_col[use_col])
            elif col_list_type in ['NonPK'] and this_col['COLUMN_KEY'] != 'PRI':
                out_col_list.append(this_col[use_col])
            elif col_list_type in ['PK'] and this_col['COLUMN_KEY'] == 'PRI':
                out_col_list.append(this_col[use_col])

        # these only get added to the column list if the proc OUTs vars the list isn't used for exec
        if (not proc.input_literals) and usage in ['define', 'example']:
            out_col_list.extend(extrees)
        elif usage == 'call': # used only by the dedupe proc call
            out_col_list.append('dupe_id')
        # print(addCols)

        return out_col_list
    def make_procs(self):
        """ iterates all of the defined procedure types and does two calls for each
        one for a version of the procedure that takes literals as input
        one for a version of the procedure that works with variables
        """

        proc_types = ['save', 'delete', 'dedupe', 'select', 'find']
        input_literals = [True, False]

        for proc_type in proc_types:
            literal_char = ''
            for literal_used in input_literals:
                if not literal_used:
                    literal_char = 'V'
                this_name = proc_type + literal_char
                self.procs[this_name] = genr8.proc.Proc(proc_type, self.macher, self, literal_used)
                self.procs[this_name].make()
                if self.macher.replace_existing:
                    if self.macher.apply_procs:
                        query = MySQL.SB.run_query('information_schema',\
                            self.procs[this_name].drop, query_type='SQL',\
                            manual_connect=self.macher.manual_connect)
                    # if replace_existing == False, this will just bomb if the proc already exists
                    if self.macher.write_procedure_files:
                        self.proc_file.write(self.procs[this_name].drop + self.macher.NEWLINE)
                if self.macher.apply_procs:
                    query = MySQL.SB.run_query('information_schema',\
                        self.procs[this_name].proc_text, query_type='SQL',\
                        manual_connect=self.macher.manual_connect)
                # delimiter changes get written to the sql file as most sql clients need them
                # they aren't used when the CREATE procs are sent directly to the DB
                if self.macher.write_procedure_files:
                    self.proc_file.write(self.delimiter_in + self.macher.NEWLINE)
                    self.proc_file.write(self.procs[this_name].proc_text)
                    self.proc_file.write(self.delimiter + self.macher.NEWLINE)
                    self.proc_file.write(self.delimiter_out + self.macher.NEWLINE)

        # close that file, if there is one
        if self.proc_file is not None:
            print('wrote stored procedures to', self.proc_file_name, end='\n\n')
            self.proc_file.close()


    def __init__(self, macher):
        self.macher = macher
        self.procs = {}
        if self.macher.write_procedure_files:
            #print()
            self.proc_file_name = os.path.join(self.macher.proc_location,\
                self.macher.table_schema + '_' + self.macher.table_name + '_' +\
                self.macher.time_str + '.sql')
            self.proc_file = open(self.proc_file_name, 'w')
        else:
            self.proc_file_name = None
            self.proc_file = None
