"""
defines the Proc class which is responsible for creating the code for
stored procedures
"""

class Proc():
    """
    properties and methods for individual procedures
    this includes...well, check __dict__ and leave me alone

    """

    proc_prefix = "_sp"
    uses_dupe_id = ['save', 'dedupe']


    def do_proc_name(self):
        """
        creates the name for the current stored procedure (both versions)
        and the current dedupe proc name
        """

        self.this_proc_name = '{self.proc_prefix}{self.proc_prefix_variblized}_{self.action}_{self.macher.table_name}'.format(**locals())
        self.variable_version = '{self.proc_prefix}V_{self.action}_{self.macher.table_name}'.format(**locals())
        self.literal_version = '{self.proc_prefix}_{self.action}_{self.macher.table_name}'.format(**locals())
        self.dedupe_proc_name = '{self.proc_prefix}V_dedupe_{self.macher.table_name}'.format(**locals()) # always the V version


    def make_dedupe_proc_call(self):
        """
        creates the call for dedupe stored procedure use
        which is referenced in other stored procedures
        """

        out_list = []
        out_list.append("""
call {self.macher.table_schema}.{self.dedupe_proc_name}(""".format(**locals()))
        out_list.append('_' + ', _'.join(self.proc_macher.make_column_list(self, 'call')))
        # now handled by the makeColumnList function
        # if self.action in self.uses_dupe_id: # add the dupe id col
        #     out += ', _dupe_id'
        out_list.append(");")
        return ''.join(out_list)


    def make_save_proc(self):
        """
        creates the active part of the save stored procedure
        """

        if self.input_literals:
            self.proc_text_list.append("DECLARE _dupe_id INT;" + self.macher.NEWLINE)
            self.proc_text_list.append("DECLARE _saved TINYINT;" + self.macher.NEWLINE)
            self.proc_text_list.append("SET _dupe_id = NULL; -- to contain an id for a duplicate value if there is one" + self.macher.NEWLINE)
        self.proc_text_list.append("SET _saved = 0; -- use this to determine whether or not the save was successful" + self.macher.NEWLINE)
        self.proc_text_list.append(self.make_dedupe_proc_call()) # reuse this for insert proc
        self.proc_text_list.append("""

IF _dupe_id IS NULL THEN

\tIF _{self.macher.ai_col} IS NULL THEN
\t-- this is an insert

\t\tinsert into {self.macher.table_schema}.{self.macher.table_name} (""".format(**locals())) # list of column names for the insert
        self.proc_text_list.append(', '.join(self.proc_macher.make_column_list(self, 'execute')))
        # now close that line
        self.proc_text_list.append(")" + self.macher.NEWLINE)
        # prefix with _
        self.proc_text_list.append('\t\tvalues (_' + ', _'.join(self.proc_macher.make_column_list(self, 'execute')))
        # now close that line
        self.proc_text_list.append(");" + self.macher.NEWLINE)

        if self.macher.ai_col is not None: # some tables use GUIDs. some have no AI. sad.
            self.proc_text_list.append("""
\t\tSELECT last_insert_id() INTO _{self.macher.ai_col};
""".format(**locals()))
        self.proc_text_list.append("""
\tELSE

\t\tUPDATE {self.macher.table_schema}.{self.macher.table_name}
\t\tSET""".format(**locals()))
        this_col_list = self.proc_macher.make_column_list(self, 'execute') # no extrees needed
        for i, col in enumerate(this_col_list):
            self.proc_text_list.append('{self.macher.NEWLINE}\t\t{col} = _{col}'.format(**locals()))
            if i + 1 < len(this_col_list): # add some commas
                self.proc_text_list.append(',')

        self.proc_text_list.append("""
        WHERE {self.macher.ai_col} = _{self.macher.ai_col};

\tEND IF;
\tSET _saved = ROW_COUNT();

END IF;
""".format(**locals()))

        if self.input_literals: # need to SELECT the values out as a select
            self.proc_text_list.append("""

SELECT _dupe_id as dupe_id, _saved as saved, _{self.macher.ai_col} as {self.macher.ai_col};

""".format(**locals()))

    def make_dedupe_proc(self):
        """
        creates the active part of the deduplication procedure
        """
        self.proc_text_list.append("DECLARE _alt_id INT;" + self.macher.NEWLINE)
        if self.input_literals:
            self.proc_text_list.append("DECLARE _dupe_id INT;" + self.macher.NEWLINE) # this doesn't need to be declared for the spV version
            self.proc_text_list.append("SET _dupe_id = NULL; -- to contain an id for a duplicate value if there is one" + self.macher.NEWLINE)
        self.proc_text_list.append("SET _alt_id = NULL;" + self.macher.NEWLINE)

        if self.macher.has_ai_col:
            self.proc_text_list.append("""
\tIF _{self.macher.ai_col} IS NULL THEN
\t-- get the max id and use that
\t\tSELECT MAX({self.macher.ai_col}) + 100 into _alt_id
\t\tFROM {self.macher.table_name};
\tELSE
\t\tSET _alt_id = _{self.macher.ai_col};
\tEND IF;

""".format(**locals()))

        for i in self.macher.unique_ind:
            self.proc_text_list.append("""
\tIF _dupe_id IS NULL THEN

\t\t\tSELECT {self.macher.ai_col} INTO _dupe_id
\t\t\tFROM {self.macher.table_schema}.{self.macher.table_name}
\t\t\tWHERE""".format(**locals()))
            for ind, col in enumerate(i): # one per column
                self.proc_text_list.append(self.macher.NEWLINE + '\t\t\t')
                if ind > 0:
                    self.proc_text_list.append('AND ')
                self.proc_text_list.append('{col} = _{col}'.format(**locals()))
            self.proc_text_list.append("""
\t\t\tAND {self.macher.ai_col} <> coalesce(_{self.macher.ai_col}, _alt_id)
\t\t\tLIMIT 1;
\t\tEND IF;""".format(**locals()))


        if self.input_literals: # need to select the values out as a select
            self.proc_text_list.append("""

SELECT _dupe_id AS dupe_id;

""")
    def make_find_proc(self):
        """
        creates the code for the active portion of the find
        procedures
        """
        if self.input_literals:
            self.proc_text_list.append("DECLARE _dupe_id INT;" + self.macher.NEWLINE)
            self.proc_text_list.append("DECLARE _saved TINYINT;" + self.macher.NEWLINE)
            self.proc_text_list.append("SET _dupe_id = NULL; -- to contain an id for a duplicate value if there is one" + self.macher.NEWLINE)
        self.proc_text_list.append("SET _saved = 0; -- use this to determine whether or not the save was successful" + self.macher.NEWLINE)
        self.proc_text_list.append(self.make_dedupe_proc_call()) # reuse this for insert proc
        self.proc_text_list.append("""

IF _dupe_id IS NULL THEN

\tinsert into {self.macher.table_schema}.{self.macher.table_name} (""".format(**locals())) # list of column names for the insert
        self.proc_text_list.append(', '.join(self.proc_macher.make_column_list(self, 'execute')))
        # now close that line
        self.proc_text_list.append(")" + self.macher.NEWLINE)
        self.proc_text_list.append('\t\tvalues (_' + ', _'.join(self.proc_macher.make_column_list(self, 'execute'))) # prefix with _
        # now close that line
        self.proc_text_list.append(");" + self.macher.NEWLINE)

        if self.macher.ai_col is not None: # some tables use GUIDs. some have no AI. sad.
            self.proc_text_list.append("""
\tSET _{self.macher.ai_col} = last_insert_id();
\tSET _saved = ROW_COUNT();
""".format(**locals()))

        self.proc_text_list.append("""
\tELSE

\t\tSET _{self.macher.ai_col} = _dupe_id;

\tEND IF;
""".format(**locals()))

        if self.input_literals: # need to SELECT the values out as a select
            self.proc_text_list.append("""

SELECT _saved as saved, _{self.macher.ai_col} as {self.macher.ai_col};

""".format(**locals()))


    def make_pk_selects(self):
        """
        used by the select procs to append columns as needed for all of the columns in the PK
        """
        for i, pk_col in enumerate(self.macher.pk_cols): #some lucky tables have more than one
            self.proc_text_list.append(self.macher.NEWLINE + '\t\t')
            if i > 0:
                self.proc_text_list.append('AND ')
            self.proc_text_list.append('{pk_col} = _{pk_col}'.format(**locals()))
        self.proc_text_list.append(";")

    def make_select_proc(self):
        """
        makes the active part of the select procedures
        """

        if not self.input_literals:
            self.proc_text_list.append("\tSELECT ")
            self.proc_text_list.append(', '.join(self.proc_macher.make_column_list(self, 'execute')))
            self.proc_text_list.append(self.macher.NEWLINE + "\tINTO" + self.macher.NEWLINE +"\t")
            self.proc_text_list.append('_' + ', _'.join(self.proc_macher.make_column_list(self, 'execute')))
            self.proc_text_list.append("""
\tFROM {self.macher.table_schema}.{self.macher.table_name}
\tWHERE""".format(**locals()))
            self.make_pk_selects()

        if self.input_literals: # need to select the vals
            self.proc_text_list.append(self.macher.NEWLINE +"\tSELECT ")
            self.proc_text_list.append(', '.join(self.proc_macher.make_column_list(self, 'execute')))
            self.proc_text_list.append("""
\tFROM {self.macher.table_schema}.{self.macher.table_name}
\tWHERE""".format(**locals()))
            self.make_pk_selects()



    def make_delete_proc(self):
        """
        creates the action parts of the delete stored procedures
        """
        if self.input_literals:
            # this doesn't need to be declared for the spV version
            self.proc_text_list.append("DECLARE _deleted TINYINT;")
        self.proc_text_list.append("""SET _deleted = 0;""")
        self.proc_text_list.append("""
\tdelete from {self.macher.table_schema}.{self.macher.table_name}
\twhere""".format(**locals()))

        for i, pk_col in enumerate(self.macher.pk_cols): #some lucky tables have more than one
            self.proc_text_list.append(self.macher.NEWLINE + '\t\t\t')
            if i > 0:
                self.proc_text_list.append('AND ')
            self.proc_text_list.append('{pk_col} = _{pk_col}'.format(**locals()))
            self.proc_text_list.append(';')
            self.proc_text_list.append(self.macher.NEWLINE + "SET _deleted = ROW_COUNT();")

            if self.input_literals: # need to select the values out as a select
                self.proc_text_list.append("""

SELECT _deleted AS deleted;

""")
    def boiler_plate(self):
        """
        takes a row of data and creates the comments
        and common stuff to all of the insert, update, del

        """

        self.drop = "DROP PROCEDURE IF EXISTS {self.macher.table_schema}.{self.this_proc_name};".format(**locals())
        self.use = "USE {self.macher.table_schema};".format(**locals())

        boiler_plate = """

CREATE DEFINER=root@localhost PROCEDURE {self.macher.table_schema}.{self.this_proc_name}(""".format(**locals())

        return boiler_plate

    def make_comments(self):
        """
        generates the comments section of stored procedures
        including a usage example
        """

        is_deterministic = '' # nothing meaning yes
        if self.action in ['select', 'dedupe']:
            is_deterministic = 'NOT '

        comments_section_list = []
        comments_section_list.append(""")
LANGUAGE SQL
{is_deterministic}DETERMINISTIC
CONTAINS SQL
SQL SECURITY INVOKER
COMMENT '{self.action}s a row in {self.macher.table_schema}.{self.macher.table_name}'
BEGIN

/*

DATE:                 {self.macher.time_str}
AUTHOR:             genr8 - automatically generated
VERSION:            0.0000001
ENVIRONMENT:    Unspecified
USAGE:                call {self.macher.table_schema}.{self.this_proc_name}(""".format(**locals()))


        comments_section_list.append('{self.example_var_prefix}'.format(**locals()) + ', {self.example_var_prefix}'.format(**locals()).join(self.proc_macher.make_column_list(self, 'example'))) # prefix with @ signs, like real variables
        comments_section_list.append(");")
        if self.input_literals:
            comments_section_list.append('{self.comment_spacer}This proc allows only for literal input.{self.comment_spacer}Use {self.variable_version} if you need to pass in variables as input{self.comment_spacer}or read them as output'.format(**locals()))
        if not self.input_literals:
            comments_section_list.append('{self.comment_spacer}This proc allows only for variable input.{self.comment_spacer}Use {self.literal_version} if you need to pass in literals as input{self.comment_spacer}or have a recordset returned as output'.format(**locals()))

        comments_section_list.append("""
*/
""")
        return ''.join(comments_section_list)

    def close_proc(self):
        """
        generates the close of the stored procedure
        used by all procs
        """

        close = """
END;
"""
        return close

    def make(self):
        """
        method for pulling together the relevant code from other functions in the
        class to create a complete stored procedure
        """

        if self.action == 'save' or self.action == 'find':
            self.extra_cols = ['dupe_id', 'saved']
            self.extra_cols_type = ['dupe_id int', 'saved tinyint']
        elif self.action == 'dedupe':
            self.extra_cols = ['dupe_id']
            self.extra_cols_type = ['dupe_id int']
        elif self.action == 'select':
            pass
        elif self.action == 'delete':
            self.extra_cols = ['deleted']
            self.extra_cols_type = ['deleted tinyint']
        else:
            msg = "unknown action " + self.action
            raise ValueError(msg)
        # these are common to all
        self.proc_text_list.append(self.boiler_plate())
        self.proc_text_list.append('{self.macher.NEWLINE}\t{self.variable_prefix} _'.format(**locals()) + ',{self.macher.NEWLINE}\t{self.variable_prefix} _'.format(**locals()).join(self.proc_macher.make_column_list(self, 'define')))    # prefix with underscores
        self.proc_text_list.append(self.make_comments())
        if self.action == 'save':
            self.make_save_proc()
        elif self.action == 'dedupe':
            self.make_dedupe_proc()
        elif self.action == 'select':
            self.make_select_proc()
        elif self.action == 'delete':
            self.make_delete_proc()
        elif self.action == 'find':
            self.make_find_proc()
        else:
            msg = "unknown action " + self.action
            raise ValueError(msg)
        # again common to all
        self.proc_text_list.append(self.close_proc())

        self.proc_text = ''.join(self.proc_text_list)

        #print(self.drop)
        #print(self.proc_text)


    def __init__(self, action, macher, proc_macher, input_literals=False):
        self.proc_text = ""
        self.proc_text_list = []
        self.macher = macher
        self.proc_macher = proc_macher
        self.extra_cols_type = []
        self.extra_cols = []
        self.input_literals = input_literals
        self.proc_prefix_variblized = "V" # variable input procs are _spV_
        self.variable_prefix = "INOUT"
        self.example_var_prefix = "@"
        if self.input_literals: # these will be passed in as variables
            self.proc_prefix_variblized = ""
            self.variable_prefix = ""
            self.example_var_prefix = ""
        self.action = action
        self.do_proc_name()
        self.comment_spacer = self.macher.NEWLINE + "                            "
        self.drop = None
        self.use = None
