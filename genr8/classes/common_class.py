"""
defines the CommonClass class which is extended by all
other classes generated by genr8
"""

from datetime import datetime
import util

class CommonClass():
    """Properties that are common to all of the generated classes

    this will need to do some stuffs
    1. validate the input for the given type, we think these will be
        str
        float
        int
        date
        take into account that None may be ok for a given value
        and shoot, do we need to translate None to NULL explicitly?
    2. return validation messaging if present.
        throw an error or throw it into an array which matches the name?
    3. set the values if they're ok
    4. update the clean & readytosave stuff
        'varchar', 'longtext', 'enum', 'text', 'char', 'mediumtext'

        'set' ,'float' ,'smallint' ,'geometry' ,'bigint' ,
        'mediumint' ,'int' ,'tinyint' ,'decimal' ,'double'

        'datetime' ,'timestamp' ,'time' ,'year'

        'longblob', 'mediumblob', 'blob'

    """

    def __automatic_dates__(self):
        """checks to see if a save operation is being done on a
            new or existing object, and if so,
            sets date_added and date_updated values if they exist and are None
"""

        right_now = datetime.now()

        has_date_added = hasattr(self, 'date_added') or hasattr(self, 'added_date')
        has_date_updated = hasattr(self, 'date_updated') or hasattr(self, 'updated_date')

        if has_date_added:
            # which var name
            added_var = 'date_added' if hasattr(self, 'date_added') else 'added_date'

            current_date_added = getattr(self, added_var)
            if not current_date_added:
                setattr(self, added_var, right_now)

        if has_date_updated:
            # which var name
            updated_var = 'date_updated' if hasattr(self, 'date_updated') else 'updated_date'
            # always update the date added
            setattr(self, updated_var, right_now)

    def __now_yr_dirty__(self):
        """Sets clean and ready_to_save to false,
            usually when setting a new value for a property using __check_and_set__
"""
        self.clean = False # changed by a setter
        self.ready_to_save = False

    def __so_fresh_and_so_clean__(self):
        """sets both clean and ready_to_save values to True,
            indicating that no values have been changed since
            the last time the data was loaded from the db
"""
        self.clean = True
        self.clean = True # changed by a setter
        self.ready_to_save = True
        self.__clear_errata__()


    def check_errata(self):
        """checks the state of the errata and throws ValueError if errors remain
"""
        if self.__errata__:
            message = str(len(self.__errata__)) + " errors found on properties. Read self.errata for details."
            raise ValueError(message)


    def __add_err__(self, attr_name, val):
        """Adds a new error value to a property if it doesn't already exist
"""
        if attr_name not in self.__errata__:
            self.__errata__[attr_name] = [val] # not in the dict yet, so add a new entry
        elif attr_name in self.__errata__:
            if val not in self.__errata__[attr_name]: # attr is in dict but value is not
                self.__errata__[attr_name].append(val)


    def __rem_err__(self, attr_name, val):
        """Removes a specific error from a property
"""
        if attr_name not in self.__errata__:
            message = attr_name + " has no errata"
            raise KeyError(message)
        elif attr_name in self.__errata__:
            self.__errata__[attr_name].remove(val)


    def __clr_err__(self, attr_name):
        """Clears all errors for a property
"""
        if attr_name in self.__errata__:
            del self.__errata__[attr_name]


    def __clear_errata__(self):
        """Clears all errors for all properties
"""
        self.__errata__ = {}

    @property
    def errata(self):
        """a dict of lists for each property which has one or more errors"""
        return self.__errata__


    @errata.setter
    def errata(self, val):
        if val != {} or (hasattr(self, '__errata__') and self.__errata__):
            message = "errata cannot be set directly. use __add_err__() or __rem_err__()"
            raise ValueError(message)
        else:
            self.__errata__ = {}


    @property
    def clean(self):
        """indicates whether the object has been loaded from the db"""
        return self.__clean


    @clean.setter
    def clean(self, val):
        if isinstance(val, bool):
            self.__clean = val
        else:
            message = str(val) + "isn't of type bool"
            raise ValueError(message)


    @property
    def ready_to_save(self):
        """indicates whether the object has been loaded from the db"""
        return self.__ready_to_save


    @ready_to_save.setter
    def ready_to_save(self, val):
        if isinstance(val, bool):
            self.__ready_to_save = val
        else:
            message = str(val) + "isn't of type bool"
            raise ValueError(message)


    def __check_and_set__(self, attr_name, public_name, val, value_type,\
        allow_none_val=False, min_val=None, max_val=None):
        """
        function for checking the values of an element to be called
            by the @property defined setter method
        this will pass None straight through
        it doesn't check whether None is allowed for the property.

        """
        change_to = None
        message = None

        if value_type == str and val is not None:
            (change_to, message) = util.validate.validate(val, 'string')
        elif value_type == int and val is not None:
            (change_to, message) = util.validate.validate(val, 'int', min_val, max_val)
        elif value_type == float and val is not None:
            (change_to, message) = util.validate.validate(val, 'float', min_val, max_val)
        elif value_type == datetime and val is not None:
            (change_to, message) = util.validate.validate(val, 'date', min_val, max_val)
        elif value_type == bool and val is not None:
            (change_to, message) = util.validate.validate(val, 'bool')
        elif not val:
            pass
        # elif type(val) == value_type:
        #    change_to = val # already the desired type
        # not implemented
        # elif value_type = 'blob'
            # thisFunc = validateBlob
        else:
            msg = 'unknown value_type specified'
            raise ValueError(msg)

        # if type(change_to) == value_type or type(change_to) == None and allow_none_val:
        # for now, skipping this validation as None is used for all init value defaults
        # which also call this method. messy!

        if not message:
            if not hasattr(self, attr_name) or change_to != getattr(self, attr_name):
                self.__now_yr_dirty__() # changed by a setter
            setattr(self, attr_name, change_to)
            self.__clr_err__(public_name)
        else:
            self.__add_err__(public_name, message)


    def __init__(self):

        # means validation has been passed and a save can be attempted
        self.__ready_to_save = False
        # means the data is in sync with the persistence layer
        self.__clean = False
        # container dict for messages about different fields
        self.__errata__ = {}
