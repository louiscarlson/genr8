"""
macher.py defines the Macher class as part of the genr8 package.

Main entry point for generating classes.

Gathers data from a MySQL DB and uses it to to generate python classes and
related stored procedures based on that schema

"""
import os
import shutil
import datetime
import util
import dbhelper.my_sql as MySQL
import genr8.proc_macher
import genr8.class_macher


class Macher():
    """ a class to make classes and stored procedures from a MySQL table
"""
    NEWLINE = '\n'

    CURRENTDIR = os.path.dirname(genr8.__file__)

    def get_table_data(self):
        """retrieves all of the column information for the specified table
        """

        this_query = """
select
c.TABLE_SCHEMA, c.TABLE_NAME, COLUMN_NAME,
concat(COLUMN_NAME, ' ', DATA_TYPE, coalesce(concat('(', CHARACTER_MAXIMUM_LENGTH, ')'), ''), case when COLUMN_TYPE like '%unsigned' then ' unsigned' else '' end) as COL_W_DATA,
COLUMN_KEY, EXTRA, IS_NULLABLE,
case when IS_NULLABLE = 'YES' then 'True'
else 'False' end as python_is_nullable,
ordinal_position, COLUMN_COMMENT,
case when DATA_TYPE like '%signed%' then 1 else 0 end as is_signed, COLUMN_DEFAULT,
case when DATA_TYPE IN ('varchar', 'longtext', 'enum', 'text', 'char', 'mediumtext') then '\\''
 when DATA_TYPE IN ('set' ,'float' ,'smallint' ,'geometry' ,'bigint' ,'mediumint' ,'int' ,'tinyint' ,'decimal' ,'double') then ''
 when DATA_TYPE IN ('datetime' ,'timestamp' ,'time' ,'year') then '\\''
 when DATA_TYPE IN ('longblob', 'mediumblob', 'blob') then '\\''
 else '' end as TEXT_QUALIFIER,
case when DATA_TYPE IN ('varchar', 'longtext', 'enum', 'text', 'char', 'mediumtext') then 'str'
 when DATA_TYPE IN ('bit', 'set' ,'smallint' , 'bigint' ,'mediumint' ,'int' ,'tinyint') then 'int'
 when DATA_TYPE IN ('DECIMAL', 'DEC', 'NUMERIC', 'FIXED', 'FLOAT', 'DOUBLE', 'DOUBLE PRECISION', 'REAL') then 'float'
 when DATA_TYPE IN ('datetime' ,'timestamp' ,'time' ,'year') then 'datetime'
 when DATA_TYPE IN ('longblob', 'mediumblob', 'blob') then 'blob'
 else '' end as pythonType, CHARACTER_MAXIMUM_LENGTH, NUMERIC_SCALE, NUMERIC_PRECISION,
 t.TABLE_COMMENT,
    case
when DATA_TYPE LIKE 'BIT%' and COLUMN_TYPE not like '%unsigned' then -128
when DATA_TYPE LIKE '%' and COLUMN_TYPE     like '%unsigned' then 0
when DATA_TYPE LIKE 'TINYINT%' and COLUMN_TYPE not like '%unsigned' then -128
when DATA_TYPE LIKE '%' and COLUMN_TYPE     like '%unsigned' then 0
when DATA_TYPE LIKE 'SMALLINT%' and COLUMN_TYPE not like '%unsigned' then -32768
when DATA_TYPE LIKE '%' and COLUMN_TYPE     like '%unsigned' then 0
when DATA_TYPE LIKE 'MEDIUMINT%' and COLUMN_TYPE not like '%unsigned' then -8388608
when DATA_TYPE LIKE '%' and COLUMN_TYPE     like '%unsigned' then 0
when DATA_TYPE LIKE 'INT%' and COLUMN_TYPE not like '%unsigned' then -2147483648
when DATA_TYPE LIKE '%' and COLUMN_TYPE     like '%unsigned' then 0
when DATA_TYPE LIKE 'INTEGER%' and COLUMN_TYPE not like '%unsigned' then -2147483648
when DATA_TYPE LIKE '%' and COLUMN_TYPE     like '%unsigned' then 0
when DATA_TYPE LIKE 'BIGINT%' and COLUMN_TYPE not like '%unsigned' then -9223372036854770000
when DATA_TYPE LIKE '%' and COLUMN_TYPE     like '%unsigned' then 0
else NULL end as minVal,
case
when DATA_TYPE LIKE 'BIT%' and COLUMN_TYPE    not like '%unsigned' then 127
when DATA_TYPE LIKE 'BIT%' and COLUMN_TYPE     like '%unsigned' then 255
when DATA_TYPE LIKE 'TINYINT%' and COLUMN_TYPE    not like '%unsigned' then 127
when DATA_TYPE LIKE 'TINYINT%' and COLUMN_TYPE     like '%unsigned' then 255
when DATA_TYPE LIKE 'SMALLINT%' and COLUMN_TYPE    not like '%unsigned' then 32767
when DATA_TYPE LIKE 'SMALLINT%' and COLUMN_TYPE     like '%unsigned' then 65535
when DATA_TYPE LIKE 'MEDIUMINT%' and COLUMN_TYPE    not like '%unsigned' then 8388607
when DATA_TYPE LIKE 'MEDIUMINT%' and COLUMN_TYPE     like '%unsigned' then 16777215
when DATA_TYPE LIKE 'INT%' and COLUMN_TYPE    not like '%unsigned' then 2147483647
when DATA_TYPE LIKE 'INT%' and COLUMN_TYPE     like '%unsigned' then 4294967295
when DATA_TYPE LIKE 'INTEGER%' and COLUMN_TYPE    not like '%unsigned' then 2147483647
when DATA_TYPE LIKE 'INTEGER%' and COLUMN_TYPE     like '%unsigned' then 4294967295
when DATA_TYPE LIKE 'BIGINT%' and COLUMN_TYPE    not like '%unsigned' then 9223372036854775807
when DATA_TYPE LIKE 'BIGINT%' and COLUMN_TYPE     like '%unsigned' then 18446744073709551615
when DATA_TYPE IN ('varchar', 'longtext', 'enum', 'text', 'char', 'mediumtext') then CHARACTER_MAXIMUM_LENGTH
else null end as maxVal
from information_schema.columns c
join information_schema.tables t
on c.table_schema = t.table_schema
and c.table_name = t.table_name
where c.table_name = '{self.table_name}'
and c.table_schema = '{self.table_schema}'
order by ordinal_position;
        """.format(**locals())

        self.dat = MySQL.SB.run_query('information_schema',\
            this_query, query_type='SQL', manual_connect=self.manual_connect)

        if not self.dat.results: # i see nothing
            msg = "{self.table_name} not found in {self.table_schema}".format(**locals())
            raise ValueError(msg)

        for col in self.dat.results:
            if col['EXTRA'] == 'auto_increment': # yay we have an ai column
                self.ai_col = col['COLUMN_NAME']
                self.has_ai_col = True
            if col['COLUMN_KEY'] == 'PRI':
                self.pk_cols.append(col['COLUMN_NAME'])


    def get_unique_constraints(self):
        """looks up all of the columns involvd in unique constraints within the table
        this data is used both in the class module and the stored procedures
        """

        self.unique_ind = []

        this_query = """
SELECT t.`name` AS table_name, i.`name` AS index_name ,
GROUP_CONCAT(f.name ORDER BY f.pos) AS Columns
FROM information_schema.innodb_sys_tables t
JOIN information_schema.innodb_sys_indexes i USING (table_id)
JOIN information_schema.innodb_sys_fields f USING (index_id)
WHERE t.name = '{self.table_schema}/{self.table_name}'
and TYPE in (2,3)
GROUP BY 1,2;
""".format(**locals())

        self.dat_ind = MySQL.SB.run_query('information_schema',\
            this_query, query_type='SQL', manual_connect=self.manual_connect)

        for index in self.dat_ind.results:
            this_index_cols = index['Columns'].split(',')
            # auto increment cols don't need testing by dedupe
            if len(this_index_cols) == 1 and this_index_cols[0] != self.ai_col:
                self.unique_ind.append(this_index_cols) # add to unique indices
            elif len(this_index_cols) > 1:
                # loop through and test we didn't sneak in any ai cols, if not then add else barf
                addme = True
                for ind_col in this_index_cols:
                    if ind_col == self.ai_col:
                        addme = False
                        break
                if addme:
                    self.unique_ind.append(this_index_cols)
                else:
                    msg = 'Unique index contains multiple cols {Columns} one of which is an AI column.'.format_map(index)
                    raise ValueError(msg)


    def __init__(self, table_name, table_schema, class_location,
                 replace_existing=True, apply_procs=True, write_procedure_files=True,
                 manual_connect=None, proc_location=None):
        self.time = datetime.datetime.now()
        self.time_str = self.time.strftime("%Y.%m.%d.%H.%M.%S")
        self.table_name = table_name
        self.table_schema = table_schema
        self.column_list = []
        self.pk_cols = [] # making this a list because, you know pk can be > 1 col in some cases
        self.ai_col = None
        self.has_ai_col = False
        self.class_location = class_location
        self.manual_connect = manual_connect
        self.get_table_data()
        self.get_unique_constraints()
        self.replace_existing = replace_existing
        self.apply_procs = apply_procs
        self.write_procedure_files = write_procedure_files
        self.proc_location = proc_location
        if self.write_procedure_files:
            self.time_str_file = self.time.strftime("%Y_%m_%d_%H_%M_%S")
            self.proc_file_name = os.path.join(proc_location, table_schema +\
                '_' + table_name + '_' + self.time_str + '.sql')
            self.proc_file = open(self.proc_file_name, 'w')
        if self.dat.results:
            # self.make_column_list()
            self.proc_macher = genr8.proc_macher.ProcMacher(self)
            self.proc_macher.make_procs()
            self.class_macher = genr8.class_macher.ClassMacher(self)
            self.class_macher.make_class()
            # check to see if the common_class is in the chosen dir. the classes all need it
            common_class_path = os.path.join(self.class_location, 'common_class.py')
            source_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'classes', 'common_class.py')
        if not os.path.exists(common_class_path):
            shutil.copyfile(source_path, common_class_path)
            print('copied common_class.py to', self.class_location)


def main():
    """gets input from the user about which schema to use, and if needed
    gets db connection info using dbhelper"""

    # x = macher('twitter_status', 'tw')

    # get schema and connection info from user
    # dbhelper may already have a config file, so this could just be choosing one of those

    use_config_file = False
    force_manual_input = False
    in_conn = None # passed to dbhelper if the config file is used instead

    if not hasattr(MySQL.config, 'CONFIG_FILE'):
        # there is no sb, as happens when a config file isn't specified
        force_manual_input = True
    else:
        print('the config file', MySQL.config.CONFIG_FILE, 'was found in this installation')
        use_config_file = util.getInput.getInput('bool', 'use this file', defaultVal="Y")

    if not use_config_file:
        print('using manual connection information')
        in_conn = MySQL.manual_connect_input()

    schemas_query = 'select schema_name from information_schema.SCHEMATA;'
    schemas = MySQL.SB.run_query('information_schema',\
        schemas_query, query_type='SQL', manual_connect=in_conn)
    schema_list = []
    for schemata in schemas.results:
        schema_list.append(schemata['schema_name'])

    # schemas.dump()

    print('found schemas', ', '.join(schema_list))

    schema = util.getInput.getInput('enum', 'generate code for schema', allowedValues=schema_list)
    this_schema_query = """select * from information_schema.tables
where table_schema = '{schema}' AND TABLE_TYPE = 'BASE TABLE';
""".format(**locals())

    tables = MySQL.SB.run_query('information_schema',\
        this_schema_query, query_type='SQL', manual_connect=in_conn)

    in_class_location = util.getInput.getInput('dir', 'directory for generated classes')

    print('if replaced, existing class files will be renamed.')

    in_replace_existing = util.getInput.getInput('bool',\
        'replace existing procedures and classes', defaultVal='Y')

    in_write_procedure_files = util.getInput.getInput('bool',\
        'write stored procedures to file', defaultVal='Y')

    in_proc_location = None

    if in_write_procedure_files:
        in_proc_location = util.getInput.getInput('dir', 'directory for stored procedure files')

    if not tables.results:
        print('there are no tables in', schema)
    else:
        for table in tables.results:
            Macher(table['TABLE_NAME'], schema, manual_connect=in_conn,\
                class_location=in_class_location, replace_existing=in_replace_existing,\
                write_procedure_files=in_write_procedure_files, proc_location=in_proc_location)


if __name__ == "__main__":
    main()
