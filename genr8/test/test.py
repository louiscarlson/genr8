import genr8
from dbhelper import my_sql as MySQL
import os


if __name__ == "__main__":
    # x = macher('twitter_status', 'tw')

    schema = 'tw'
    thisQ = """select * from information_schema.tables where table_schema = '{schema}';
""".format(**locals())

    tables = MySQL.SB.run_query('information_schema', thisQ, query_type = 'SQL')

    for z in tables.results:
        class_path = os.getcwd() + '\\..\\classes'
        proc_path = os.getcwd() + '\\..\\procedures'
        print(class_path)
        x = genr8.macher.Macher(z['TABLE_NAME'], schema, class_location=class_path, proc_location=proc_path)