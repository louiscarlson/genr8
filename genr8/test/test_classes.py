# pylint: disable=E1101
# disabling this specific check as many dynamically created variables are used

"""test generates python classes and procedures for a test schema
IMPORTANT:
before using this, add _test_schema_ to the config file used
by dbhelper with all relevant credentials
needs to have drop and create schema as well as information_schema privs
"""

import os
import importlib
import unittest
import time
import sys
import genr8
from dbhelper import my_sql as MySQL

sys.path.append("classes")


class ClassTests(unittest.TestCase):
    """unit test suite for genr8"""

    @classmethod
    def tearDownClass(cls):
        print('running teardown')


    @classmethod
    def setUpClass(cls):
        """does all of the actual stored procedure and class generation but
        has to be done before the unit tests on those classes can be run"""
        print('running setUpClass')
        cls.SCHEMA_NAME = "_test_schema_"
        cls.TABLE_NAME = 'test_table'

        make_schema_query = 'create schema ' + cls.SCHEMA_NAME + ';'
        cls.drop_schema_query = 'drop schema if exists ' + cls.SCHEMA_NAME + ';'

        MySQL.SB.run_query('information_schema', cls.drop_schema_query, query_type='SQL')
        MySQL.SB.run_query('information_schema', make_schema_query, query_type='SQL')

        make_example_table_query = """

    create table {cls.SCHEMA_NAME}.{cls.TABLE_NAME} (
        id int unsigned not null primary key auto_increment COMMENT 'trivial key',
        test_table_value varchar(50) not null COMMENT 'some value',
        added_date datetime not null COMMENT 'date and time the record was added',
        updated_date datetime not null COMMENT 'date and time the record was updated'
        
    ) COMMENT 'just a test';

    """.format(**locals())
        MySQL.SB.run_query('information_schema', make_example_table_query, query_type='SQL')

        make_index_table_query = """
        alter table {cls.SCHEMA_NAME}.{cls.TABLE_NAME} add unique index test_table_value (test_table_value);
        """.format(**locals())

        MySQL.SB.run_query('information_schema', make_index_table_query, query_type='SQL')

        all_tables_query = """select * from information_schema.tables where table_schema = '{cls.SCHEMA_NAME}';
    """.format(**locals())

        tables = MySQL.SB.run_query('information_schema', all_tables_query, query_type='SQL')

        for tab in tables.results:
            class_path = os.getcwd() + '\\classes'
            proc_path = os.getcwd() + '\\procedures'
            # print(class_path)
            genr8.macher.Macher(tab['TABLE_NAME'], cls.SCHEMA_NAME, class_location=class_path, proc_location=proc_path)

        file_name = cls.SCHEMA_NAME + '_' + cls.TABLE_NAME
        # print(file_name)
        # this is needed to help find common_class referenced in the imported classes
        # module_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'classes', file_name)

        imported = importlib.import_module('classes.' + file_name)
        cls.this_class = getattr(imported, file_name)

        # print(locals())

    def test_insert(self):
        """should insert a unique new row and the auto-increment should increase by 1"""
        # an example value
        test_value = 'abc'
        max_query = 'select max(id) as max_id from ' + self.TABLE_NAME + ';'
        max_lookup = MySQL.SB.run_query(self.SCHEMA_NAME, max_query, query_type='SQL')
        instance_1 = self.this_class(test_table_value=test_value)
        instance_1.find()
        print('instance id', instance_1.id, 'max_id', max_lookup.results[0]['max_id'])
        self.assertEqual(instance_1.id, max_lookup.results[0]['max_id'] + 1)

    def test_update(self):
        """tests that a record is updated and that the udpated_date is changed"""
        test_value = 'def'
        instance_1 = self.this_class(test_table_value=test_value)
        instance_1.find()
        original_updated = instance_1.updated_date
        # print(instance_1.id)
        test_value_2 = 'xyz'
        instance_1.test_table_value = test_value_2
        time.sleep(1)
        instance_1.save()
        instance_1.load()
        print(instance_1.test_table_value)
        self.assertEqual(instance_1.test_table_value, test_value_2)
        self.assertNotEqual(original_updated, instance_1.updated_date)

    def test_duplicate(self):
        """using find to insert a row, then using save to try to create a duplicate
        should throw an exception"""
        test_value = 'aaa'
        instance_1 = self.this_class(test_table_value=test_value)
        instance_1.find()
        # print(instance_1.id)
        instance_2 = self.this_class(test_table_value=test_value)
        with self.assertRaises(Exception) as context:
            instance_2.save()
        self.assertTrue('create a duplicate' in str(context.exception))
        # self.assertEqual(instance_1.test_table_value, instance_2.test_table_value)

    def test_delete(self):
        """creates, then deletes a row"""
        test_value = 'zzz'
        instance_1 = self.this_class(test_table_value=test_value)
        instance_1.find()
        self.assertIsNotNone(instance_1.id)
        instance_1.delete()
        self.assertIsNone(instance_1.id)
        instance_2 = self.this_class(test_table_value=test_value)
        self.assertIsNone(instance_2.dedupe())

    def test_dedupe(self):
        """creates, then deletes a row"""
        test_value = 'argh'
        instance_1 = self.this_class(test_table_value=test_value)
        instance_1.find()
        self.assertIsNotNone(instance_1.id)
        instance_2 = self.this_class(test_table_value=test_value)
        self.assertEqual(instance_1.id, instance_2.dedupe())

    def test_load(self):
        """saves a new row, then loads another instance with the same id"""
        test_value = 'load'
        instance_1 = self.this_class(test_table_value=test_value)
        instance_1.find()
        self.assertIsNotNone(instance_1.id)
        instance_2 = self.this_class(id=instance_1.id)
        self.assertEqual(instance_2.test_table_value, test_value)


def main():
    """ runs the unit tests"""

    unittest.main(verbosity=3)



if __name__ == "__main__":
    main()
