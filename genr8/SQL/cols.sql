    select
    *

    from information_schema.columns
    where table_name = 'twitter_status'
    and table_schema = 'tw'
    order by ordinal_position;

    select
    TABLE_SCHEMA, TABLE_NAME, COLUMN_NAME,
    concat(COLUMN_NAME, ' ', DATA_TYPE, coalesce(concat('(', CHARACTER_MAXIMUM_LENGTH, ')'), '')) as COL_W_DATA,
    COLUMN_KEY, EXTRA, IS_NULLABLE

    from information_schema.columns
    where table_name = 'twitter_status'
    and table_schema = 'tw'
    order by ordinal_position;

    /*
    index type col
    A numeric identifier signifying the kind of index. 0 = Secondary Index, 1 = Clustered Index, 2 = Unique Index, 3 = Primary Index, 32 = Full-text Index.
    */


    select * from information_schema.innodb_sys_tables t ;

    select * from information_schema.innodb_sys_indexes where table_id = 77;

    select * from information_schema.tables
    where table_schema = 'information_schema';


    SELECT t.name AS `Table`,
             i.name AS `Index`,
             GROUP_CONCAT(f.name ORDER BY f.pos) AS `Columns`
FROM information_schema.innodb_sys_tables t
JOIN information_schema.innodb_sys_indexes i USING (table_id)
JOIN information_schema.innodb_sys_fields f USING (index_id)
WHERE t.schema = 'tw'
GROUP BY 1,2;