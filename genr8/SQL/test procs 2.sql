

insert into tw.twitter_handle(twitter_handle, date_added, date_updated)
values ('bob', now(), now());


select * from tw.twitter_handle;

call tw._sp_insert_twitter_status(124, 1, 'testing', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, now(), now());

call tw._sp_insert_twitter_status(123, 1, 'testing', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-08-24', '2017-08-24');

select * from tw.twitter_status;


call tw._sp_update_twitter_status(2, 222, 1, 'updating2', 2, 2, 2, 2, 2, 2, 2, 2, now(), now());

call tw._sp_delete_twitter_status(6);


SET @twitter_status_id = 4, @deleted = 0;


call tw._spV_delete_twitter_status(@twitter_status_id, @deleted);

select @twitter_status_id, @deleted;

call tw._sp_select_twitter_status(2);



SET @orig_twitter_status_id = 1000;
SET @twitter_handle_id = 1;
SET @twitter_status = 'blargh';
SET @char_count = 9;
SET @word_count = 9;
SET @word_found_count = 9;
SET @url_count = 9;
SET @handle_count = 9;
SET @emoji_count = 9;
SET @tag_count = 9;
SET @image_count = 9;
SET @date_added = now();
SET @date_updated = now();

call tw._sp_insert_twitter_status(@orig_twitter_status_id,    @twitter_handle_id,    @twitter_status,    @char_count,    @word_count,    @word_found_count,    @url_count,    @handle_count,    @emoji_count,    @tag_count,    @image_count,    @date_added,    @date_updated);

select * from tw.twitter_status;


SET @twitter_status_id = NULL;
SET @is_dupe = NULL;
SET @inserted = NULL;

SET @orig_twitter_status_id = 1001;

call tw._spV_insert_twitter_status(@twitter_status_id, @orig_twitter_status_id,    @twitter_handle_id,    @twitter_status,
@char_count,    @word_count,    @word_found_count,    @url_count,    @handle_count,    @emoji_count,
@tag_count,    @image_count,    @date_added,    @date_updated, @is_dupe, @inserted);

select @twitter_status_id,    @is_dupe, @inserted;



SET @orig_twitter_status_id = 2000;
SET @twitter_handle_id = 1;
SET @twitter_status = 'updating now';
SET @char_count = 10;
SET @word_count = 10;
SET @word_found_count = 10;
SET @url_count = 10;
SET @handle_count = 10;
SET @emoji_count = 10;
SET @tag_count = 10;
SET @image_count = 10;
SET @date_added = now();
SET @date_updated = now();


SET @twitter_status_id = 8;


select * from tw.twitter_status;

call tw._sp_update_twitter_status(@twitter_status_id, @orig_twitter_status_id,    @twitter_handle_id,    @twitter_status,    @char_count,    @word_count,    @word_found_count,    @url_count,    @handle_count,    @emoji_count,    @tag_count,    @image_count,    @date_added,    @date_updated);



SET @twitter_status_id = 7;

SET @orig_twitter_status_id = 9000;
SET @twitter_handle_id = 1;
SET @twitter_status = 'updating now 9';
SET @char_count = 90;
SET @word_count = 90;
SET @word_found_count = 90;
SET @url_count = 90;
SET @handle_count = 90;
SET @emoji_count = 90;
SET @tag_count = 90;
SET @image_count = 90;
SET @date_added = now();
SET @date_updated = now();
SET @dupe_id = NULL;
SET @updated = NULL;
call tw._spV_update_twitter_status(@twitter_status_id, @orig_twitter_status_id,    @twitter_handle_id,    @twitter_status,
@char_count,    @word_count,    @word_found_count,    @url_count,    @handle_count,    @emoji_count,
@tag_count,    @image_count,    @date_added,    @date_updated, @dupe_id, @updated);

select @twitter_status_id,    @dupe_id, @updated;
SET @is_duplicate = NULL;
SET @dupe_id = NULL;

call tw._spV_dedupe_twitter_status(@twitter_status_id, @orig_twitter_status_id,    @twitter_handle_id,    @twitter_status,
@char_count,    @word_count,    @word_found_count,    @url_count,    @handle_count,    @emoji_count,
@tag_count,    @image_count,    @date_added,    @date_updated, @dupe_id, @is_duplicate);

select @twitter_status_id,    @dupe_id, @is_duplicate;

            SELECT twitter_status_id INTO @dupe_id
            FROM tw.twitter_status
            WHERE
            orig_twitter_status_id = @orig_twitter_status_id
            AND twitter_status_id <> COALESCE(@twitter_status_id, -1)
            LIMIT 1;


            select @dupe_id;


set @_twitter_handle_id = NULL;
set @_twitter_handle = 'blargh';
set @_date_added = now();
set @_date_updated = now();
call tw._sp_find_twitter_handle(@_twitter_handle_id, @_twitter_handle, @_date_added, @_date_updated);
select @_twitter_handle_id, @_twitter_handle, @_date_added, @_date_updated;


set @_twitter_handle_id = NULL;
set @_twitter_handle = 'blargh3';
set @_date_added = now();
set @_date_updated = now();
set @_dupe_id = NULL;
set @_saved = NULL;
call tw._spV_find_twitter_handle(@_twitter_handle_id, @_twitter_handle, @_date_added, @_date_updated, @_dupe_id, @_saved);
select @_twitter_handle_id, @_twitter_handle, @_date_added, @_date_updated, @_dupe_id, @_saved;


select * from tw.twitter_handle;



set @_twitter_status_id = NULL;
set @_orig_twitter_status_id = 124;
set @_twitter_handle_id = 1;
set @_twitter_status = 'blargh';
set @_char_count = 0;
set @_word_count = 0;
set @_word_found_count = 0;
set @_url_count = 0;
set @_handle_count = 0;
set @_emoji_count = 0;
set @_tag_count = 0;
set @_image_count = 0;
set @_date_added = now();
set @_date_updated = now();
set @_dupe_id = NULL;
set @_saved = NULL;
call tw._spV_find_twitter_status(@_twitter_status_id, @_orig_twitter_status_id, @_twitter_handle_id, @_twitter_status, @_char_count, @_word_count, @_word_found_count, @_url_count, @_handle_count, @_emoji_count, @_tag_count, @_image_count, @_date_added, @_date_updated, @_dupe_id, @_saved);
select @_twitter_status_id, @_orig_twitter_status_id, @_twitter_handle_id, @_twitter_status, @_char_count, @_word_count, @_word_found_count, @_url_count, @_handle_count, @_emoji_count, @_tag_count, @_image_count, @_date_added, @_date_updated, @_dupe_id, @_saved;


set @_twitter_status_id = 5;
set @_orig_twitter_status_id = 124;
set @_twitter_handle_id = 1;
set @_twitter_status = 'blargh';
set @_char_count = 0;
set @_word_count = 0;
set @_word_found_count = 0;
set @_url_count = 0;
set @_handle_count = 0;
set @_emoji_count = 0;
set @_tag_count = 0;
set @_image_count = 0;
set @_date_added = now();
set @_date_updated = now();
set @_dupe_id = NULL;
set @_saved = NULL;
call tw._spV_find_twitter_status(@_twitter_status_id, @_orig_twitter_status_id, @_twitter_handle_id, @_twitter_status, @_char_count, @_word_count, @_word_found_count, @_url_count, @_handle_count, @_emoji_count, @_tag_count, @_image_count, @_date_added, @_date_updated, @_dupe_id, @_saved);
select @_twitter_status_id, @_orig_twitter_status_id, @_twitter_handle_id, @_twitter_status, @_char_count, @_word_count, @_word_found_count, @_url_count, @_handle_count, @_emoji_count, @_tag_count, @_image_count, @_date_added, @_date_updated, @_dupe_id, @_saved;






            