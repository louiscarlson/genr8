from setuptools import setup

setup(
    name = 'genr8',
    version = '1.1.0',
    description = 'modules for generating MySQL procedures and python classes from schema',
    author = 'T. What',
    author_email = 'thinkywhatsit@gmail.com',
    packages = ['genr8'],
    install_requires=[
                "util",
                "dbhelper",
        ]
    )